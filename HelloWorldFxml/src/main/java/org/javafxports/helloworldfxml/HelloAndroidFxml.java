package org.javafxports.helloworldfxml;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class HelloAndroidFxml extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        System.out.println("HelloAndroidFxml start()");
        final Parent root = FXMLLoader.load(getClass().getResource("MainView.fxml"));
        final Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
        stage.setScene(new Scene(root, visualBounds.getWidth(), visualBounds.getHeight()));
        stage.show();
    }
}
