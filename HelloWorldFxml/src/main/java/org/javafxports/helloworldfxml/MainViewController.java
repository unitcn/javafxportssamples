/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javafxports.helloworldfxml;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Region;

public class MainViewController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button button;
    @FXML
    private Region region;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked the button.");
        label.setText("You clicked the button.");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        assert label  != null : "fx:id=\"label\" was not injected: check your FXML file 'MainView.fxml'.";
        assert button != null : "fx:id=\"button\" was not injected: check your FXML file 'MainView.fxml'.";
        assert region != null : "fx:id=\"rectangle\" was not injected: check your FXML file 'MainView.fxml'.";
    }    
    
}
