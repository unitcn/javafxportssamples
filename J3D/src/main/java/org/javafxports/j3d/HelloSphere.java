package org.javafxports.j3d;

import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Sphere;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class HelloSphere extends Application {

    @Override
    public void start (Stage stage) throws Exception {
        int cnt = 0;
        Screen primaryScreen = Screen.getPrimary();
        Rectangle2D visualBounds = primaryScreen.getVisualBounds();
        double width = visualBounds.getWidth();
        double height = visualBounds.getHeight();
        Sphere s = new Sphere(30);
        
        Rectangle rectangle = new Rectangle(width-20, height-20);
        rectangle.setFill(Color.LIGHTBLUE);
        rectangle.setArcHeight(6);
        rectangle.setArcWidth(6);
        StackPane stackPane = new StackPane();
        stackPane.getChildren().addAll(rectangle, s);
        Scene scene = new Scene (stackPane, visualBounds.getWidth(), visualBounds.getHeight());
       
        stage.setScene(scene);
        stage.show();
    }


}
